package com.jaf.andecam.presentation.listing.view;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaf.andecam.R;
import com.jaf.andecam.presentation.detail.view.TruckDetailActivity;
import com.jaf.andecam.presentation.listing.adapter.TruckListingAdapter;
import com.jaf.andecam.presentation.listing.model.ProductListingViewModel;
import com.jaf.andecam.presentation.utils.GridSpacingItemDecoration;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListingFragment extends Fragment implements TruckListingAdapter.TruckListingCallbacks {

    public static final String KEY_DATASET = "DATASET";
    @BindView(R.id.productList)
    protected RecyclerView recyclerView;

    public static ProductListingFragment newInstance(List<ProductListingViewModel> dataSet) {
        ProductListingFragment fragment = new ProductListingFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_DATASET, (Serializable) dataSet);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_listing, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);

        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        List<ProductListingViewModel> serializable = (List<ProductListingViewModel>) getArguments().getSerializable(KEY_DATASET);
        TruckListingAdapter adapter = new TruckListingAdapter(serializable);
        adapter.addTruckListener(this);
        recyclerView.setAdapter(adapter);

    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onTruckSelected(int position) {
        startActivity(TruckDetailActivity.newIntent(getContext(), position));
    }
}