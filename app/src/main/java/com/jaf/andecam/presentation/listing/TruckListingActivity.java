package com.jaf.andecam.presentation.listing;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.jaf.andecam.R;
import com.jaf.andecam.presentation.listing.adapter.ProductsTabAdapter;

public class TruckListingActivity extends AppCompatActivity {

    private ProductsTabAdapter productsTabAdapter;

    private ViewPager viewPager;
    private TabLayout tabLayout;

    public static Intent newIntent(Context from) {
        return new Intent(from, TruckListingActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_listing);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        productsTabAdapter = new ProductsTabAdapter(getSupportFragmentManager());
        bindViews();
        setupView();
    }

    private void setupView() {
        setupTabs();
        setupViewPager();
    }

    private void bindViews() {
        viewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
    }

    private void setupViewPager() {
        viewPager.setAdapter(productsTabAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void setupTabs() {
        tabLayout.addTab(tabLayout.newTab().setText("CONSTELLATION"));
        tabLayout.addTab(tabLayout.newTab().setText("DELIVERY"));
        tabLayout.addTab(tabLayout.newTab().setText("WORKER"));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_product_listing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
