package com.jaf.andecam.presentation.listing.adapter;


import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaf.andecam.R;
import com.jaf.andecam.presentation.listing.model.ProductListingViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class TruckListingAdapter extends Adapter<TruckListingAdapter.ViewHolder> {

    private final List<ProductListingViewModel> dataSet;
    private TruckListingCallbacks listener;

    public TruckListingAdapter(List<ProductListingViewModel> dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public TruckListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.truck_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductListingViewModel viewModel = dataSet.get(position);
        Random rn = new Random();
        int answer = rn.nextInt(6) + 1;
        holder.thumbnail.setImageResource(map.get(answer));
        holder.title.setText(viewModel.truckName());

        holder.thumbnail.setOnClickListener(view -> {
            notifyTruckSelected(position);
        });
    }

    private void notifyTruckSelected(int position) {
        if(listener!= null) {
            listener.onTruckSelected(position);
        }
    }

    public static HashMap<Integer, Integer> map ;
    static {
        map = new HashMap<>();
        map.put(1, R.drawable.constellation_19420);
        map.put(2, R.drawable.constellation_truck);
        map.put(3, R.drawable.constellation_5);
        map.put(4, R.drawable.constellation_10);
        map.put(5, R.drawable.constellation_19360);
        map.put(6, R.drawable.constellation_17280_tractor);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView thumbnail;

        private ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            thumbnail = (ImageView) v.findViewById(R.id.thumbnail);
        }
    }
    public void addTruckListener(TruckListingCallbacks listingCallbacks) {
        listener = listingCallbacks;
    }

    public interface TruckListingCallbacks {
        void onTruckSelected(int position);
    }

}

