package com.jaf.andecam.presentation;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.jaf.andecam.presentation.listing.TruckListingActivity;
import com.jaf.andecam.R;

public class SplashActivity extends Activity {

    public static final int SPLASH_DURATION = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupFullScreen();
        setContentView(R.layout.activity_splash);
        finishSplash();
    }

    private void setupFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void finishSplash() {
        new Handler().postDelayed(this::goToProductListing, SPLASH_DURATION);
    }

    private void goToProductListing() {
        startActivity(TruckListingActivity.newIntent(this).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }
}
