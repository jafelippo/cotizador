package com.jaf.andecam.presentation.detail.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.jaf.andecam.R;
import com.jaf.andecam.presentation.listing.adapter.TruckListingAdapter;

import java.util.Random;


public class TruckDetailActivity extends AppCompatActivity {

    public static final String EXTRA_TRUCK_ID = "truck_id";

    public static Intent newIntent(Context from, Integer truckId) {
        Intent intent = new Intent(from, TruckDetailActivity.class);
        Bundle params = new Bundle();
        params.putInt(EXTRA_TRUCK_ID, truckId);
        intent.putExtras(params);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_truck_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Cummins / ISL 360");

        loadBackdrop();
    }

    private void loadBackdrop() {
        final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        Random rn = new Random();
        int answer = rn.nextInt(6) + 1;
        imageView.setImageResource(TruckListingAdapter.map.get(answer));;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
