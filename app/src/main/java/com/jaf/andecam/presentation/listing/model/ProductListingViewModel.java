package com.jaf.andecam.presentation.listing.model;


import java.io.Serializable;

public class ProductListingViewModel implements Serializable{

    private final String truckName;
    private final String base64TruckImage;

    public ProductListingViewModel(String truckName, String base64TruckImage) {
        this.truckName = truckName;
        this.base64TruckImage = base64TruckImage;
    }

    public String truckName() {
        return truckName;
    }

    public String truckImage() {
        return base64TruckImage;
    }
}
